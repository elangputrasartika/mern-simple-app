import React, { Component } from 'react';

class UserCreate extends Component {
    constructor(props) {
        super(props);

        this.state = {
            nik: "",
            nama: "",
            alamat: "",
            no_telepon: "",
            email: "",
            unit: "",
            status: "",
            role: ""
        }
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmit = (e) => {
        e.preventDefault();

        this.setState({
            nik: "",
            nama: "",
            alamat: "",
            no_telepon: "",
            email: "",
            unit: "",
            status: "",
            role: ""
        })
    }

    render() {
        return (
            <div className={"container"}>
                <div style={{marginTop: 10}}>
                    <h3>Create New User</h3>
                    <form onSubmit={this.onSubmit}>
                        <div className="form-group">
                            <label>NIK: </label>
                            <input  type="text"
                                    className="form-control"
                                    name={"nik"}
                                    value={this.state.nik}
                                    onChange={this.onChange}
                            />
                        </div>
                        <div className="form-group">
                            <label>Nama: </label>
                            <input
                                type="text"
                                className="form-control"
                                name={"nama"}
                                value={this.state.nama}
                                onChange={this.onChange}
                            />
                        </div>
                        <div className="form-group">
                            <label>Alamat: </label>
                            <input
                                type="text"
                                className="form-control"
                                name={"alamat"}
                                value={this.state.alamat}
                                onChange={this.onChange}
                            />
                        </div>
                        <div className="form-group">
                            <label>No Telepon: </label>
                            <input
                                type="text"
                                className="form-control"
                                name={"no_telepon"}
                                value={this.state.no_telepon}
                                onChange={this.onChange}
                            />
                        </div>
                        <div className="form-group">
                            <label>Email: </label>
                            <input
                                type="email"
                                className="form-control"
                                name={"email"}
                                value={this.state.email}
                                onChange={this.onChange}
                            />
                        </div>
                        <div className="form-group">
                            <label>Unit: </label>
                            <input
                                type="text"
                                className="form-control"
                                name={"unit"}
                                value={this.state.unit}
                                onChange={this.onChange}
                            />
                        </div>
                        <div className="form-group">
                            <label>Status: </label>
                            <select
                                className="form-control"
                                name={"status"}
                                value={this.state.status}
                                onChange={this.onChange}
                            >
                                <option value={"ACTIVE"}>AKTIF</option>
                                <option value={"NOT ACTIVE"}>TIDAK AKTIF</option>
                            </select>
                        </div>
                        <div className="form-group">
                            <label>Role: </label>
                            <br/>
                            <div className="form-check form-check-inline">
                                <input  className="form-check-input"
                                        type="radio"
                                        name="role"
                                        value="user"
                                        checked={this.state.role === 'user'}
                                        onChange={this.onChange}
                                />
                                <label className="form-check-label">User</label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input  className="form-check-input"
                                        type="radio"
                                        name="role"
                                        value="admin"
                                        checked={this.state.role === 'admin'}
                                        onChange={this.onChange}
                                />
                                <label className="form-check-label">Admin</label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input  className="form-check-input"
                                        type="radio"
                                        name="role"
                                        value="superadmin"
                                        checked={this.state.role==='superadmin'}
                                        onChange={this.onChange}
                                />
                                <label className="form-check-label">Superadmin</label>
                            </div>
                        </div>

                        <div className="form-group float-right">
                            <input type="submit" value="Create User" className="btn btn-primary" />
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default UserCreate;
