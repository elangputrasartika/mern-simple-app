import React, { Component } from "react";
import logo from "./logo.svg";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import './App.css';

import UserCreate from "./components/pages/user-create";
import UserEdit from "./components/pages/user-edit";
import UserList from "./components/pages/user-list";

class App extends Component {
    render() {
        return (
            <Router>
                <div className="container-fluid p-0">
                    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                        <Link to="/" className="navbar-brand">
                            <img src={logo} width="30" height="30" alt={"Simple API App"}/> User Manager App
                        </Link>
                        <div className="collpase navbar-collapse">
                            <ul className="navbar-nav mr-auto">
                                <li className="navbar-item">
                                    <Link to="/" className="nav-link">User</Link>
                                </li>
                                <li className="navbar-item">
                                    <Link to="/create" className="nav-link">Create User</Link>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <br/>
                    <Switch>
                        <Route path="/" exact component={UserList} />
                        <Route path="/edit/:id" component={UserEdit} />
                        <Route path="/create" component={UserCreate} />
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;
