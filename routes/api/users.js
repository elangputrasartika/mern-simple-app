const express = require('express');

const router = express.Router();
const path = require('path');
const { UserController } = require('../index');
// const { catchErrors } = require('../../middleware/error-handler');

router.get('/users', UserController.getUsers)
    .post('/create', UserController.createUser);
// router.post('/create', UserController.createUser);
router.use(function(req, res) {
    res.sendFile(path.join(__dirname, '../../client/build/index.html'));
});
console.log(path.join(__dirname, '../../client/build/index.html'))
module.exports = router;
