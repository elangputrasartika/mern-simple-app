const mongoose = require('mongoose');

const { Schema } = mongoose;
let User = new Schema({
    nik: {
        type: String
    },
    alamat: {
        type: String
    },
    no_telepon: {
        type: String
    },
    email: {
        type: String
    },
    unit: {
        type: String
    },
    status: {
        type: String
    },
    role: {
        type: String
    },
});

module.exports = mongoose.model('User', User, 'users');

