const Validator = require('validator');
const isEmpty = require('is-empty');

module.exports = function validateCreateUser(request) {
    const errors = {};
    const data = request;

    data.nama = !isEmpty(data.nama) ? data.nama : '';
    if (Validator.isEmpty(data.nama) || data.nama === undefined) {
        errors.name = 'Nama tidak boleh kosong';
    }
    if (Validator.isEmpty(data.nik) || data.nik === undefined) {
        errors.nik = 'NIK tidak boleh kosong';
    }
    if (Validator.isEmpty(data.alamat) || data.alamat === undefined) {
        errors.alamat = 'Alamat tidak boleh kosong';
    }
    if (Validator.isEmpty(data.no_telepon) || data.no_telepon === undefined) {
        errors.no_telepon = 'Nomor telepon tidak boleh kosong';
    }
    if (Validator.isEmpty(data.email) || data.email === undefined) {
        errors.email = 'Email tidak boleh kosong';
    }
    if (Validator.isEmpty(data.unit) || data.unit === undefined) {
        errors.unit = 'Unit tidak boleh kosong';
    }
    if (Validator.isEmpty(data.status) || data.status === undefined) {
        errors.status = 'Status tidak boleh kosong';
    }
    if (Validator.isEmpty(data.role) || data.role === undefined) {
        errors.role = 'Role tidak boleh kosong';
    }

    return {
        errors: errors,
        isValid: isEmpty(errors),
    };
};
