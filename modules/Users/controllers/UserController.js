const mongoose = require("mongoose");
const User = require('../models/Users');
const UserRepo = require("../repos/UserRepo");
const validateCreateUser = require('../validators/createUser');


exports.getUsers = async (req, res) => {
    try {
        const { page = 1, limit = 10 } = req.query;
        const result = await UserRepo.getAll(page, limit);

        return res.status(200).json({
            status: "success",
            metadata: result[0].metadata,
            data: result[0].data
        });
    } catch (e) {
        return res.status(400).json({ status: "error", message: e });
    }
};

exports.createUser = async (req, res) => {
    try {
        const { nama, nik, alamat, no_telepon, email, unit, status, role } = req.body;
        const isUserExists = await User.find({ nik: nik });

        const { errors, isValid } = validateCreateUser(req.body);
        if (!isValid) {
            return res.status(400).json(errors);
        } else if (isUserExists.length > 0) {
            return res.status(400).json({status: "error", message: "NIK sudah digunakan."});
        }

        const user = await User.create({
            nama: nama,
            nik: nik,
            alamat: alamat,
            no_telepon: no_telepon,
            email: email,
            unit: unit,
            status: status,
            role: role
        });

        return res
            .status(200)
            .json({ message: 'User berhasil ditambahkan', success: true, status: "succes" });
    } catch (e) {
        return res.status(400).json({ status: "error", message: e, success: false });
    }
};

